## установка Vagrant & Virtualbox

Установить на хостовую машину следующие пакеты:

	- vagrant
	- virtualbox
	- virtualbox-guest-utils

Добавить пользователя в группу vboxsf, vboxusers.

```bash
    $ sudo gpasswd vboxusers -a $USER
    $ sudo gpasswd vboxsf -a $USER
```

После установки vagrant'a установить плагин vagrant-vbguest `vagrant plugin install vagrant-vbguest --plugin-version 0.29`.

После установки virtualbox перезагрузить хостовую машину.

## Ansible test

```bash
    $ ansible-galaxy role install -r requirements. yml
    $ vagrant up
```

потребуется пересоздать example/play_vars/secret-vars.yml
